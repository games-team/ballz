/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "datawriter.hpp"
#include "resourcehandler.hpp"
#include "guichan/allegro.hpp"
#include <stdlib.h>

DataWriter::DataWriter() {
	mLettersWritten = 0;
    mBallzImage = ResourceHandler::getInstance()->getBitmap("ballz.bmp");
    mTheProfessorImage = ResourceHandler::getInstance()->getBitmap("theprofessor.bmp");
    mTheColonelImage = ResourceHandler::getInstance()->getBitmap("thecolonel.bmp");
    mFrameCounter = 0;
}

void DataWriter::logic()
{
    mFrameCounter++;
}

void DataWriter::draw(gcn::Graphics *graphics) {

    if (mText.length() == 0) {
		return;
	}
	
    int y;
	for (y = 0; y < getHeight(); y++) 
    {
        int x;
        for (x = 0; x < getWidth(); x++)
        {
            if (x % 4 == 0 || y % 4 == 0)
            {
                graphics->setColor(gcn::Color(50, 50, 50));
		        graphics->drawPoint(x, y);
            }
            if (x % 4 == 1 || y % 4 == 1)
            {
                graphics->setColor(gcn::Color(60, 60, 60));
		        graphics->drawPoint(x, y);
            }
            if (x % 4 == 2 || y % 4 == 2)
            {
                graphics->setColor(gcn::Color(40, 40, 40));
		        graphics->drawPoint(x, y);
            }
        }
	}

	gcn::Font *f = getFont();
	graphics->setFont(f);

	std::string textLeft = mText.substr(0, mLettersWritten);
	int rownum = 0;

	while (true) {
		int i = f->getStringIndexAt(textLeft, getWidth() - 84);
		int end = textLeft.length();
		if (i < end - 1) {
			end = textLeft.rfind(" ", i);
		}
		std::string row = textLeft.substr(0, end);
		if ((size_t)end != textLeft.length()) {
			graphics->drawText(row, 64, f->getHeight() * rownum + 8);
		} else {
			graphics->drawText(row + "_", 64, f->getHeight() * rownum + 8);
		}
		rownum++;
		if ((size_t)end == textLeft.length()) {
			break;
		}
		textLeft = textLeft.substr(end + 1, textLeft.length() - end - 1);
	}

    gcn::AllegroGraphics* allegroGraphics = static_cast<gcn::AllegroGraphics*>(graphics);

    if (mImage == BALLZ)
    {
        gcn::AllegroImage image(mBallzImage, 0);
        allegroGraphics->drawImage(&image, 0, 0, 8, 8, image.getWidth(),
            image.getHeight());
    }
    else if (mImage == THEPROFESSOR)
    {
        gcn::AllegroImage image(mTheProfessorImage, 0);
        allegroGraphics->drawImage(&image, 0, 0, 8, 8, image.getWidth(),
            image.getHeight());
    }
    else if (mImage == THECOLONEL)
    {
        gcn::AllegroImage image(mTheColonelImage, 0);
        allegroGraphics->drawImage(&image, 0, 0, 8, 8, image.getWidth(),
            image.getHeight());
    }

    unsigned int i;
    for (i = 0; i < 256; i++)
    {
        int color = rand() % 255;
        allegroGraphics->setColor(gcn::Color(color, color, color));
        allegroGraphics->drawPoint(8 + (rand() % 48), 8 + (rand() % 48)); 
    }

    int color = rand() % 255;
    int initY = 0;//8 + (rand() % 48);
    allegroGraphics->setColor(gcn::Color(color, color, color));
    allegroGraphics->drawLine(8, 
                              initY + 8 + (mFrameCounter / 2 % 48),
                              55,
                              initY + 8 + (mFrameCounter / 2 % 48));
}

void DataWriter::setText(const std::string &text) {
    if (text.length() <= 1)
    {
        mText = text;
        return;
    }

	mText = text.substr(2, text.length() - 2);

    if (text.at(0) == 'B')
    {
        mImage = BALLZ;
    }
    else if (text.at(0) == 'P')
    {
        mImage = THEPROFESSOR;
    }
    else if (text.at(0) == 'C')
    {
        mImage = THECOLONEL;
    }
}

const std::string &DataWriter::getText() {
	return mText;
}

void DataWriter::setLettersWritten(size_t lettersWritten) {
	if (lettersWritten >= mText.length()) {
		lettersWritten = mText.length();
	}
	mLettersWritten = lettersWritten;
}

size_t DataWriter::getLettersWritten() {
	return mLettersWritten;
}

bool DataWriter::isEverythingWritten() {
	return mLettersWritten == mText.length();
}
