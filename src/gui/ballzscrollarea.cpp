/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <guichan.hpp>

#include "gui/ballzscrollarea.hpp"

BallzScrollArea::BallzScrollArea()
{
	setScrollPolicy(SHOW_NEVER,	SHOW_ALWAYS);
	addKeyListener(this);
	setFocusable(false);
	setFrameSize(0);
}

void BallzScrollArea::draw(gcn::Graphics *graphics)
{
    drawChildren(graphics);

	if (getVerticalMaxScroll() != 0)
	{
		int y = ((getHeight() - 32) * getVerticalScrollAmount()) /
			getVerticalMaxScroll();

		graphics->setColor(0x000000);
		graphics->drawRectangle(gcn::Rectangle(getWidth()-11, y, 8, 32));
		graphics->drawRectangle(gcn::Rectangle(getWidth()-10, y+1, 8, 32));

		graphics->setColor(0xffffff);

		graphics->fillRectangle(gcn::Rectangle(getWidth()-10, y+1, 6, 30));
	}
}
