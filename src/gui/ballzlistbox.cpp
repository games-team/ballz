/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <guichan.hpp>

#include "gui/ballzlistbox.hpp"
#include "resourcehandler.hpp"
#include "guichan/allegro.hpp"

BallzListBox::BallzListBox()
{
	setFrameSize(0);
    mSelectedImage = ResourceHandler::getInstance()->getBitmap("hand.bmp");
}

BallzListBox::~BallzListBox()
{

}

void BallzListBox::draw(gcn::Graphics* graphics)
{
    gcn::AllegroGraphics* allegroGraphics = static_cast<gcn::AllegroGraphics*>(graphics);

	if (mListModel == NULL)
	{
		return;
	}

	allegroGraphics->setColor(getForegroundColor());
	allegroGraphics->setFont(getFont());

	int i, fontHeight;
	int y = 0;

	fontHeight = getFont()->getHeight();

	for (i = 0; i < mListModel->getNumberOfElements(); ++i)
	{
		allegroGraphics->drawText(mListModel->getElementAt(i), 10, y);

		if (i == mSelected)
		{
			if (isFocused())
			{
			        gcn::AllegroImage image(mSelectedImage, 0);
			        allegroGraphics->drawImage(&image, 0, 0, 0,
			          y + 1, image.getWidth(), image.getHeight());
			}
		}

		y += fontHeight;
	}
}

void BallzListBox::setSelected(int i)
{
    /*
	if (i >= 0 &&
			i < getListModel()->getNumberOfElements() &&
			getListModel()->getElementAt(i) == "")
	{
		if (i < getSelected())
		{
			i--;
		}
		else
		{
			i++;
		}
	}
    */
	
    gcn::ListBox::setSelected(i);
}
