/**
 * Mark unused variables, so as to leverage GCC's "unused parameter"
 * warnings.
 * 
 * Marked unused variables can be found among others in virtual or
 * inherited methods.
 * 
 * Cf. http://sourcefrog.net/weblog/software/languages/C/unused.html
*/


#ifndef BALLZ_UNUSED_HPP
#define BALLZ_UNUSED_HPP

#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define UNUSED(x) /*@unused@*/ x
#else
# define UNUSED(x) x
#endif

#endif
