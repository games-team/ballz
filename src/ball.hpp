/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BALLZ_BALL_HPP
#define BALLZ_BALL_HPP

#include <allegro.h>

#include "tilemap.hpp"

class Ball {
public:
	Ball(int x, int y);

	void logic(TileMap *tileMap);

	void draw(BITMAP *dest, int scroll);

	int getCenterX();

	int getCenterY();

	bool isMetal();

	void kill();

	void setPosition(int x, int y);

	int getMetalBarPosition();

	enum State {
		AIR_DOWN,
		AIR_UP,
		ROLL,
		DEAD
	};

	State getState();

	void freeze(int time);

    void setImortal(bool imortal) { mImortal = imortal; }

private:

	void logicWater(TileMap *tileMap, bool keyUp);
	
	void drawBall(BITMAP *dest, int x, int y, int scroll);

	int x, y;
	int dx, dy;
	bool lastKeyUp;
	bool lastKeyCtrl;
	bool upHeldDuringJump;
	bool bouncing;
	int bounceY;
	int metalBallCounter;
	int speedCooldown;
    int blinkCounter;
	int freezeTime;
    bool mImortal;

	State state;

	void setState(State newState);

	BITMAP *sprite;
    BITMAP *spriteBlink;
    BITMAP *spriteIron;
    BITMAP *spriteBlinkIron;
    BITMAP *spriteCoolDown;
    BITMAP *spriteBlinkCoolDown;

	SAMPLE *bounceSample;
	SAMPLE *rushSample;
	SAMPLE *jumpSample;
};

#endif
