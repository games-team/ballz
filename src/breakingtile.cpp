/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include "breakingtile.hpp"
#include "ball.hpp"
#include "resourcehandler.hpp"
#include "levelhandler.hpp"
#include "unused.hpp"

BreakingTile::BreakingTile(int x, int y) 
: Tile(x, y)
{
	state = 0;
    mDesertTile = ResourceHandler::getInstance()->getBitmap("breakingtile.bmp");
    mSewerTile = ResourceHandler::getInstance()->getBitmap("breakingtilesewer.bmp");
    mPrisonTile = ResourceHandler::getInstance()->getBitmap("breakingtileprison.bmp");
}

void BreakingTile::draw(BITMAP *dest, int frame, int scroll)
{
	frame = state / 5;
	if (frame < 4) 
    {
        int level = LevelHandler::getInstance()->getCurrentLevel();
        LevelModel* levelModel = LevelHandler::getInstance()->getLevel(level);

        if (levelModel->tileSet == 0)
        {
            frame = frame % (mDesertTile->w / 16);
	        masked_blit(mDesertTile, dest, frame * 16, 0, getX() - scroll, getY(), 16, 16);
        }
        else if (levelModel->tileSet == 1)
        {
            frame = frame % (mSewerTile->w / 16);
	        masked_blit(mSewerTile, dest, frame * 16, 0, getX() - scroll, getY(), 16, 16);
        }
        else if (levelModel->tileSet == 2)
        {
            frame = frame % (mPrisonTile->w / 16);
	        masked_blit(mPrisonTile, dest, frame * 16, 0, getX() - scroll, getY(), 16, 16);
        }
	}
}

bool BreakingTile::isSolid()
{
	return state < 13;
}

void BreakingTile::collision(Level* UNUSED(level), Ball* ball)
{
	if (state != 0) {
		return;
	}

	if (ball->getCenterY() - 8 < getY())		
	{
		if (state == 0) {
			state = 1;
		}
	}
}

void BreakingTile::logic()
{
	if (state > 0 && state < 20)
	{
		state++;
	}
}
